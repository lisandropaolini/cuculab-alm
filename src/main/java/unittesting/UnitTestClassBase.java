package unittesting;

import java.net.URI;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TestWatcher;

import com.hp.lft.report.Reporter;
import com.hp.lft.report.Status;
import com.hp.lft.sdk.ModifiableSDKConfiguration;
import com.hp.lft.sdk.SDK;
import com.hp.lft.unittesting.UnitTestBase;

import tsoft.LeanFtTest;

public class UnitTestClassBase extends UnitTestBase {

    protected static UnitTestClassBase instance;

    public static void globalSetup(UnitTestClassBase instance2) throws Exception {
        try {
            ModifiableSDKConfiguration config = new ModifiableSDKConfiguration();
             config.setServerAddress(new URI("ws://localhost:5095"));
             SDK.init(config);
             Reporter.init();
             
            if (instance == null)
                instance = new LeanFtTest();
            //instance.classSetup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Before
    public void beforeTest() throws Exception {
        testSetup();
    }

    @After
    public void afterTest() throws Exception {
    }

    public static void globalTearDown() throws Exception {
        try {
             instance.classTearDown();
             getReporter().generateReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ClassRule
    public static TestWatcher classWatcher = new TestWatcher() {
        @Override
        protected void starting(org.junit.runner.Description description) {
            className = description.getClassName();
        }
    };

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        @Override
        protected void starting(org.junit.runner.Description description) {
            testName = description.getMethodName();
        }

        @Override
        protected void failed(Throwable e, org.junit.runner.Description description) {
            setStatus(Status.Failed, e);
            testTearDownNoThrow();
        }

        @Override
        protected void succeeded(org.junit.runner.Description description) {
            setStatus(Status.Passed);
            testTearDownNoThrow();
        }
    };

    @Override
    protected String getTestName() {
        return testName;
    }

    @Override
    protected String getClassName() {
        return className;
    }

    protected static String className;
    protected String testName;

}