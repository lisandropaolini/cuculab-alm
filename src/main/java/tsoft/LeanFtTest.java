package tsoft;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Assert;
import unittesting.UnitTestClassBase;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"junit:junitResult.xml"}, features = "src/main/java/tsoft")
public class LeanFtTest extends UnitTestClassBase {

	public LeanFtTest() {
		// Change this constructor to private if you supply your own public constructor
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		instance = new LeanFtTest();
		globalSetup(instance);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		globalTearDown();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() throws Exception{
		Assert.assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
		
}