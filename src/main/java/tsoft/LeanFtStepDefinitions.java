package tsoft;

import org.junit.Assert;

import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;
import com.hp.lft.sdk.web.Button;
import com.hp.lft.sdk.web.ButtonDescription;
import com.hp.lft.sdk.web.EditField;
import com.hp.lft.sdk.web.EditFieldDescription;
import com.hp.lft.sdk.web.Link;
import com.hp.lft.sdk.web.LinkDescription;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LeanFtStepDefinitions {
	Browser browser;

	// Implementation of the features steps
	@Given("^a user goes to google$")
	public void a_user_goes_to_google() throws Throwable {
		browser = BrowserFactory.launch(BrowserType.CHROME);
		browser.navigate("https://www.google.cl/");
	}

	@Given("^searches for HPE$")
	public void searches_for_HPE() throws Throwable {
		browser.describe(EditField.class,
				new EditFieldDescription.Builder().type("text").tagName("INPUT").name("q").build())
				.setValue("dockertips");

		browser.describe(Button.class,
				new ButtonDescription.Builder().buttonType("submit").tagName("INPUT").name("Buscar con Google").build())
				.click();
	}

	@When("^he clicks on the link$")
	public void he_clicks_on_the_link() throws Throwable {
		browser.describe(Link.class, new LinkDescription.Builder().tagName("a").innerText("DockerTips").build())
				.click();
	}

	@Then("^the page should appear$")
	public void the_page_should_appear() throws Throwable {
		Assert.assertTrue(!browser.getTitle().isEmpty());
	}

}